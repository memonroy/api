//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var movimientosJSON = require('./movimientosv2.json')
//Guardar en una variable el archivo

var bodyParser = require('body-parser')
app.use(bodyParser.json())
//enviar jsons a través del body

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//     ---PETICION  GET
app.get('/', function (req,res) {
    //res.send('Hola Mundo NodeJS')
    res.sendFile(path.join(__dirname,'index.html'))
    //senfFile HTML
})

//     ---PETICION  POST
app.post('/', function (req,res){
    res.send('Petición recibida modificada')
})

//     ---PETICION  PUT
app.put('/', function (req,res){
    res.send('Put recibid')
})


//     ---PETICION  DELETE
app.delete('/', function (req,res){
    res.send('Delete recibido')
})

//     ---PETICION  GET CON PARAMS
app.get('/Clientes/:idCliente',function(req,res) {
//recursos en plurales y en mayúsculas
    res.send('Aqui tiene al cliente: ' + req.params.idCliente)
    //a través del objeto params del request accedes a los datos de entrada.
})

//     ---PETICION  GET TODOS LOS CLIENTES
app.get('/Movimientos',function(req,res) {
//recursos en plurales y en mayúsculas
    res.sendfile('movimientosv1.json')
    //distintos métodos File y file.
    //file json
})

//     ---PETICION  GET TODOS LOS CLIENTES
app.get('/v2/Movimientos',function(req,res) {
    res.json(movimientosJSON)
})

//     ---PETICION  GET CON PARAMS CLIENTES
app.get('/v2/Movimientos/:id',function(req,res) {
    console.log(req.params.id)
    //mostrar en consola
    res.send(movimientosJSON[req.params.id-1])
    //Solo un registro de todo el JSON, el registro que esté en el PARAM del ID
})

//     ---PETICION  GET CON QUERY PARAMS CLIENTES
app.get('/v2/Movimientosq',function(req,res) {
    console.log(req.query)
    res.send('Dato Recibido ' + req.query)
    //[object Object] mapa de objetos

})

app.post('/v2/Movimientos', function (req,res){
    var nuevo = req.body
    //viene en el objeto de la petición

    nuevo.id = movimientosJSON.length + 1
    //en el arreglo + 1
    movimientosJSON.push(nuevo)
    //push del elemento
    res.send("Movimiento dado de alta")
    //confirmacion
})


//sudo docker pull node

//sudo docker pull node:slim

//B CRIPT
